package com.bastiyan.gtech.gtechcore;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class GtechCoreApplication implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(GtechCoreApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(GtechCoreApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        log.info("Start GtechCoreApplication...");
    }

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }
}
