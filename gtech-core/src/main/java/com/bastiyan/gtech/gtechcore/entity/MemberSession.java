/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bastiyan.gtech.gtechcore.entity;

import com.bastiyan.gtech.gtechgateway.infra.IDGen;
import com.bastiyan.gtech.gtechgateway.model.LoginResponse;
import java.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author iBas
 */
@Entity
public class MemberSession {
    @Id
    private String id;
    
    private String memberId;
    
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    
    private LocalDateTime createdTimestamp;
    private LocalDateTime lastAccessTimestamp;
    private int expiryMinutes;
    private LocalDateTime expiryTimestamp;

    public MemberSession() {
    }
    
    public MemberSession(Member member, LocalDateTime createdTimestamp, int expiryMinutes) {
        this.id = IDGen.generate();
        this.memberId = member.getMemberId();
        this.firstName = member.getFirstName();
        this.lastName = member.getLastName();
        this.email = member.getEmail();
        this.phone = member.getPhone();

        this.createdTimestamp = createdTimestamp;
        this.lastAccessTimestamp = createdTimestamp;
        this.expiryTimestamp = createdTimestamp.plusMinutes(expiryMinutes);
        this.expiryMinutes = expiryMinutes;
    }
    
    public void setLastAccessTimestamp(LocalDateTime lastAccessTimestamp) {
        this.lastAccessTimestamp = lastAccessTimestamp;
        this.expiryTimestamp = createdTimestamp.plusMinutes(expiryMinutes);
    }
    
    public boolean isExpiry(LocalDateTime now) {
        return expiryTimestamp.isBefore(now);
    }
    
    public String getId() {
        return id;
    }

    public String getMemberId() {
        return memberId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }
    
    public LocalDateTime getLastAccessTimestamp() {
        return lastAccessTimestamp;
    }
    
    public LocalDateTime getCreatedTimestamp() {
        return createdTimestamp;
    }

    public LocalDateTime getExpiryTimestamp() {
        return expiryTimestamp;
    }

    public int getExpiryMinutes() {
        return expiryMinutes;
    }
    
    public LoginResponse toLoginResponse(){
        LoginResponse response = new LoginResponse();
        response.setEmail(this.email);
        response.setFirstName(this.firstName);
        response.setLastName(this.lastName);
        response.setMemberId(this.memberId);
        response.setPhone(this.phone);
        response.setSessionId(this.id);
        response.setExpiryTimestamp(this.expiryTimestamp);
        return response;
    }
}
