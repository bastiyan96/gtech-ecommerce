/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bastiyan.gtech.gtechcore.entity;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

import com.bastiyan.gtech.gtechgateway.infra.IDGen;
import com.bastiyan.gtech.gtechgateway.model.MemberRequest;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.util.Random;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Transient;

/**
 *
 * @author iBas
 */
@Entity
@NamedQueries({
        @NamedQuery(name = "Member.findByEmailOrPhone", query = "SELECT m FROM Member m WHERE m.email = :username OR m.phone = :username")
})
public class Member {
    private static final int HASH_ITERATION = 2;
    private static final String HASH_ALGO = "SHA-256";
    
    @Id
    private String memberId;
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String gender;
    private LocalDateTime dateOfBirth;
    private String passwordHash;
    private String passwordSalt;

    public Member() {
        this.memberId = IDGen.generate();
    }
    
    public Member(MemberRequest request){
        this();
        this.firstName = request.getFirstName();
        this.lastName = request.getLastName();
        this.email = request.getEmail();
        this.phone = request.getPhone();
        this.gender = request.getGender();
        this.dateOfBirth = request.getDateOfBirth();
        this.setPassword(request.getPassword());
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public LocalDateTime getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDateTime dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }
    
    public boolean verifyPassword(String password) {
        return hashPassword(password).equals(passwordHash);
    }

    private String hashPassword(String password) {
        try {
            byte[] saltBytes = Hex.decodeHex(passwordSalt.toCharArray());
            return hassPassword(password, saltBytes);
        } catch (DecoderException e) {
            throw new RuntimeException(e);
        }
    }

    private String hassPassword(String password, byte[] saltBytes) {
        try {
            byte[] passwordBytes = password.getBytes();
            MessageDigest digest = MessageDigest.getInstance(HASH_ALGO);
            digest.update(saltBytes);
            byte[] tokenHash = digest.digest(passwordBytes);
            int iterations = HASH_ITERATION - 1;
            for (int i = 0; i < iterations; i++) {
                digest.reset();
                tokenHash = digest.digest(tokenHash);
            }
            return Hex.encodeHexString(tokenHash);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public final void setPassword(String password) {
        byte[] saltBytes = new byte[16];
        new Random().nextBytes(saltBytes);

        this.passwordHash = hassPassword(password, saltBytes);
        this.passwordSalt = Hex.encodeHexString(saltBytes);
    }

    public String getPasswordSalt() {
        return passwordSalt;
    }

    
    @Override
    public String toString() {
        return "Member{" + "memberId=" + memberId + ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email + ", phone=" + phone + ", gender=" + gender + ", dateOfBirth=" + dateOfBirth + '}';
    }
    
    
}
