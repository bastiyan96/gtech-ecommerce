/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bastiyan.gtech.gtechcore.endpoint;

import com.bastiyan.gtech.gtechcore.entity.Product;
import com.bastiyan.gtech.gtechcore.repository.ProductRepository;
import com.bastiyan.gtech.gtechgateway.infra.ErrorMessage;
import com.bastiyan.gtech.gtechgateway.model.ProductModel;
import com.bastiyan.gtech.gtechgateway.model.ProductModelWrapper;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

/**
 *
 * @author iBas
 */
@RestController
@RequestMapping("/product")
public class ProductController {
    @Autowired
    private ProductRepository productRepository;
    
    @Autowired
    private ModelMapper modelMapper;
    
    @PostMapping(consumes = "application/json", produces = "application/json")
    public ProductModel register(@RequestBody ProductModel productModel) {
        Product product = new Product(productModel);
        productRepository.save(product);
        productModel.setProductId(product.getProductId());
        return productModel;
    }
    
    @GetMapping(consumes = "application/json", produces = "application/json")
    public ProductModelWrapper productList() {
        List<Product> productList = productRepository.findAll();
        List<ProductModel> productModelList = productList
                .stream()
                .map(product -> modelMapper.map(product, ProductModel.class))
                .collect(Collectors.toList());

        return new ProductModelWrapper(productModelList);
    }
    
    @GetMapping(path = "/{productId}", consumes = "application/json", produces = "application/json")
    public ProductModel getProductById(@PathVariable(value = "productId") String productId) {
        Optional<Product> product = productRepository.findById(productId);
        if(product.isPresent()){
            return modelMapper.map(product.get(), ProductModel.class);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ErrorMessage.NOT_FOUND);
        }
    }
    
    @PostMapping(path = "/update", consumes = "application/json", produces = "application/json")
    public ProductModel update(@RequestBody ProductModel productModel) {
        Optional<Product> product = productRepository.findById(productModel.getProductId());
        if(product.isPresent()){
            Product selectedProduct = product.get();
            if(!selectedProduct.getMemberId().equals(productModel.getMemberId())){
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ErrorMessage.INVALID_PERMISSION);
            }
            selectedProduct.setBrandName(productModel.getBrandName());
            selectedProduct.setCategoryNames(productModel.getCategoryNames());
            selectedProduct.setProductCode(productModel.getProductCode());
            selectedProduct.setProductDescrption(productModel.getProductDescrption());
            selectedProduct.setProductName(productModel.getProductName());
            selectedProduct.setProductPrice(productModel.getProductPrice());
            selectedProduct.setStoreName(productModel.getStoreName());
            
            productRepository.save(selectedProduct);
            
            return modelMapper.map(selectedProduct, ProductModel.class);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ErrorMessage.NOT_FOUND);
        }
    }
}
