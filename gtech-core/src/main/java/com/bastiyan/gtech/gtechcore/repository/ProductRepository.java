/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bastiyan.gtech.gtechcore.repository;

import com.bastiyan.gtech.gtechcore.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author iBas
 */
public interface ProductRepository extends JpaRepository<Product, String> {
    Product findByCategoryNames(String categoryNames);
}
