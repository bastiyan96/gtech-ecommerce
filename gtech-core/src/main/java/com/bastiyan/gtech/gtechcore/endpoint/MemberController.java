/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bastiyan.gtech.gtechcore.endpoint;

import com.bastiyan.gtech.gtechcore.entity.Member;
import com.bastiyan.gtech.gtechcore.entity.MemberSession;
import com.bastiyan.gtech.gtechcore.repository.MemberRepository;
import com.bastiyan.gtech.gtechcore.repository.MemberSessionRepository;
import com.bastiyan.gtech.gtechgateway.infra.ErrorMessage;
import com.bastiyan.gtech.gtechgateway.model.LoginRequest;
import com.bastiyan.gtech.gtechgateway.model.LoginResponse;
import com.bastiyan.gtech.gtechgateway.model.MemberRequest;
import com.bastiyan.gtech.gtechgateway.model.MemberResponse;
import com.bastiyan.gtech.gtechgateway.model.MemberSessionModel;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

/**
 *
 * @author iBas
 */
@RestController
@RequestMapping("/member")
public class MemberController {

    private final static int SESSION_TIMEOUT = 15;
    private static final Logger log = LoggerFactory.getLogger(MemberController.class);
        
    @Autowired
    private MemberRepository memberRepository;
    
    @Autowired
    private ModelMapper modelMapper;
    
    @Autowired
    private MemberSessionRepository memberSessionRepository;

    @PostMapping(consumes = "application/json", produces = "application/json")
    public MemberRequest register(@RequestBody MemberRequest memberRegistrationModel) {
        if (!memberRegistrationModel.getPassword().equals(memberRegistrationModel.getConfirmPassword())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ErrorMessage.INVALID_PASSWORD_CONFIRMATION);
        }

        Member member = new Member(memberRegistrationModel);
        memberRepository.save(member);
        memberRegistrationModel.setMemberId(member.getMemberId());
        return memberRegistrationModel;
    }

    @PostMapping(path = "/login", consumes = "application/json", produces = "application/json")
    @Transactional
    public LoginResponse login(@RequestBody LoginRequest loginRequest) {
        Member member = memberRepository.findByEmailOrPhone(loginRequest.getUsername());
        if (member == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ErrorMessage.INVALID_USERNAME_PASSWORD);
        }

        if (!member.verifyPassword(loginRequest.getPassword())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ErrorMessage.INVALID_USERNAME_PASSWORD);
        }

        LocalDateTime now = LocalDateTime.now();
        MemberSession memberSession = new MemberSession(member, now, SESSION_TIMEOUT);
        memberSessionRepository.invalidateExpirySession(now);
        memberSessionRepository.save(memberSession);

        return memberSession.toLoginResponse();
    }
    
    @GetMapping(consumes = "application/json", produces = "application/json")
    public List<MemberResponse> memberList() {
        List<Member> memberList = memberRepository.findAll();
        List<MemberResponse> memberResponseList = memberList
                .stream()
                .map(user -> modelMapper.map(user, MemberResponse.class))
                .collect(Collectors.toList());

        return memberResponseList;
    }
    
    @GetMapping(path = "/{memberId}", consumes = "application/json", produces = "application/json")
    public MemberResponse getMemberById(@PathVariable(value = "memberId") String memberId) {
        Optional<Member> member = memberRepository.findById(memberId);
        if(member.isPresent()){
            return modelMapper.map(member.get(), MemberResponse.class);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ErrorMessage.NOT_FOUND);
        }
    }
    
    @PostMapping(path = "/update", consumes = "application/json", produces = "application/json")
    public MemberResponse update(@RequestBody MemberRequest memberRegistrationModel) {
        log.info("## memberRegistrationModel.getMemberId is {} ##", memberRegistrationModel.getMemberId());
        Optional<Member> member = memberRepository.findById(memberRegistrationModel.getMemberId());
        if(member.isPresent()){
            Member selectedMember = member.get();
            selectedMember.setDateOfBirth(memberRegistrationModel.getDateOfBirth());
            selectedMember.setGender(memberRegistrationModel.getGender());
            selectedMember.setFirstName(memberRegistrationModel.getFirstName());
            selectedMember.setLastName(memberRegistrationModel.getLastName());
            
            memberRepository.save(selectedMember);
            
            return modelMapper.map(selectedMember, MemberResponse.class);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ErrorMessage.NOT_FOUND);
        }
    }
    
    @GetMapping(path = "/session/validate/{sessionId}", produces = "application/json")
    public MemberSessionModel validateMemberSessionById(@PathVariable(value = "sessionId") String sessionId) {
        Optional<MemberSession> memberSession = memberSessionRepository.findById(sessionId);
        if(memberSession.isPresent() && !memberSession.get().isExpiry(LocalDateTime.now())){
            MemberSession selectedMemberSession = memberSession.get();
            selectedMemberSession.setLastAccessTimestamp(LocalDateTime.now());
            return modelMapper.map(selectedMemberSession, MemberSessionModel.class);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ErrorMessage.INVALID_SESSION);
        }
    }
    
    @GetMapping(path = "/session/get/{sessionId}", produces = "application/json")
    public MemberSessionModel getMemberSessionById(@PathVariable(value = "sessionId") String sessionId) {
        Optional<MemberSession> memberSession = memberSessionRepository.findById(sessionId);
        if(memberSession.isPresent() && !memberSession.get().isExpiry(LocalDateTime.now())){
            return modelMapper.map(memberSession.get(), MemberSessionModel.class);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ErrorMessage.INVALID_SESSION);
        }
    }
}
