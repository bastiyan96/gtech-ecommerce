/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bastiyan.gtech.gtechgateway.infra;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author iBas
 */
public final class Hashs {
    public static final String SHA_256 = "SHA-256";
    private static final String HMAC_SHA1_ALGORITHM = "HmacSHA1";

    private Hashs() {
    }

    public static String generateHash(String text) throws NoSuchAlgorithmException {
        return generateHash(SHA_256, text.getBytes());
    }

    public static String generateHash(String algo, String text) throws NoSuchAlgorithmException {
        return generateHash(algo, text.getBytes());
    }

    public static String generateHash(String algo, byte[] b) throws NoSuchAlgorithmException {
        StringBuilder result = new StringBuilder();
        MessageDigest md = MessageDigest.getInstance(algo);
        md.update(b);
        byte[] hash = md.digest();
        for (byte value : hash) {
            result.append(String.format("%02x", value));
        }
        return result.toString();
    }

    public static String calculateRFC2104HMAC(String data, String key) throws SignatureException {
        try {
            // get an hmac_sha1 key from the raw key bytes
            SecretKeySpec signingKey = new SecretKeySpec(key.getBytes(), HMAC_SHA1_ALGORITHM);

            // get an hmac_sha1 Mac instance and initialize with the signing key
            Mac mac = Mac.getInstance(HMAC_SHA1_ALGORITHM);
            mac.init(signingKey);

            // compute the hmac on input data bytes
            byte[] rawHmac = mac.doFinal(data.getBytes());

            // base64-encode the hmac
            return new String(Base64.encodeBase64(rawHmac));

        } catch (Exception e) {
            throw new SignatureException("Failed to generate HMAC : " + e.getMessage());
        }
    }
}