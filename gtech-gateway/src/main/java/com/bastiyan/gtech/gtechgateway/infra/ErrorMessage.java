/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bastiyan.gtech.gtechgateway.infra;

/**
 *
 * @author iBas
 */
public class ErrorMessage {
    public final static String INVALID_PASSWORD_CONFIRMATION = "Password dan Konfirmasi Password Tidak Sesuai";
    public final static String INVALID_USERNAME_PASSWORD = "Invalid username or password";
    public final static String NOT_FOUND = "Permintaan Tidak Ditemukan";
    public final static String INVALID_PERMISSION = "Anda Tidak Memiliki Akses";
    public final static String INVALID_SESSION = "Sesi Anda telah habis";
    
}
