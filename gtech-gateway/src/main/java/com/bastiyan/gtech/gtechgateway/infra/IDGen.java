/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bastiyan.gtech.gtechgateway.infra;

import java.util.UUID;

/**
 *
 * @author iBas
 */
public class IDGen {

    public static String generate() {
        return UUID.randomUUID().toString().replace("-", "");
    }
    
    
}
