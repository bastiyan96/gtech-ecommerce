/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bastiyan.gtech.gtechgateway.infra;

import com.bastiyan.gtech.gtechgateway.exception.AuthException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import org.apache.commons.codec.binary.Base64;
import java.util.Map;
import jdk.internal.joptsimple.internal.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 * @author iBas
 */


public class Headers {
    private static final Logger LOG = LoggerFactory.getLogger(Headers.class);
    public static final String WEB_API_KEY = "786de9f94d6d440baf1e2ec6852dc8a4";
    public static final String WEB_SECRET_KEY = "E6tXIdfNT3ALQvZQi4TT5fIE0O1jke6l";

    private Headers() {
    }

    public static Map<String, Object> create(String authorization, String channel, String date) {
        Map<String, Object> headers = new HashMap<>();
        headers.put("Authorization", authorization);
        headers.put("Date", date);
        headers.put("Channel", channel);
        return headers;
    }

    public static Map<String, Object> create(String authorization, String channel, LocalDateTime time) {
        Map<String, Object> headers = new HashMap<>();
        headers.put("Authorization", authorization);
        headers.put("Date", generateHeaderDate(time));
        headers.put("Channel", channel);
        return headers;
    }

    public static String generateHeaderDate() {
        return generateHeaderDate(LocalDateTime.now());
    }

    public static String generateHeaderDate(LocalDateTime now) {
        return DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss").format(now);
    }

    public static String generateAuth(String path, String method, String content) {
        try {
            return generateAuth(path, method, content, WEB_API_KEY, WEB_SECRET_KEY, LocalDateTime.now());
        } catch (Exception e) {
            LOG.error("Failed create authorization, {}", e.getMessage());
            throw new AuthException("Failed create authorization");
        }
    }

    public static String generateAuth(String path, String method, String key, String secret, String content) {
        try {
            return generateAuth(path, method, content, key, secret, LocalDateTime.now());
        } catch (Exception e) {
            LOG.error("Failed create authorization, {}", e.getMessage());
            throw new AuthException("Failed create authorization");
        }
    }

    public static String generateAuth(String path,
                                      String method,
                                      String content,
                                      String key,
                                      String secret,
                                      LocalDateTime time) throws NoSuchAlgorithmException, SignatureException {
        return "Bearer " + key + ":" + generateSignature(path, method, content, secret, time);
    }

    public static String generateSignature(String path,
                                           String method,
                                           String content,
                                           String secret,
                                           LocalDateTime time) throws NoSuchAlgorithmException, SignatureException {
        StringBuilder signatureBuilder = new StringBuilder();
        signatureBuilder.append(method).append("\n");

        if (method.equalsIgnoreCase("POST") || method.equalsIgnoreCase("PUT")) {
            if (!Strings.isNullOrEmpty(content)) {
                signatureBuilder.append(new String(Base64.encodeBase64(content.getBytes()))).append("\n");
            }
        }

        signatureBuilder.append("application/json").append("\n");
        signatureBuilder.append(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss").format(time)).append("\n");
        signatureBuilder.append(path);
        String signatureDigest = signatureBuilder.toString();

        LOG.debug(signatureDigest);

        String auth = Hashs.calculateRFC2104HMAC(signatureDigest, Hashs.generateHash(secret));

        LOG.debug("Signature: {}", auth);
        return auth;
    }
}