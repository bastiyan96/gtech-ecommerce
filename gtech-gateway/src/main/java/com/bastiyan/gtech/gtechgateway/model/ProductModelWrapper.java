/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bastiyan.gtech.gtechgateway.model;

import java.util.List;

/**
 *
 * @author iBas
 */
public class ProductModelWrapper {
    private List<ProductModel> productModelList;

    public ProductModelWrapper() {
    }

    public ProductModelWrapper(List<ProductModel> productModelList) {
        this.productModelList = productModelList;
    }

    public List<ProductModel> getProductModelList() {
        return productModelList;
    }

    public void setProductModelList(List<ProductModel> productModelList) {
        this.productModelList = productModelList;
    }
    
    
}
