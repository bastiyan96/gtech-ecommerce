/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bastiyan.gtech.gtechgateway.model;

import java.math.BigDecimal;

/**
 *
 * @author iBas
 */
public class ProductModel {
    private String productId;
    private String productName;
    private String productCode;
    private BigDecimal productPrice;
    private String storeName;
    private String brandName;
    private String categoryNames;
    private String productDescrption;
    private String memberId;

    public ProductModel() {
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public BigDecimal getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(BigDecimal productPrice) {
        this.productPrice = productPrice;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getCategoryNames() {
        return categoryNames;
    }

    public void setCategoryNames(String categoryNames) {
        this.categoryNames = categoryNames;
    }

    public String getProductDescrption() {
        return productDescrption;
    }

    public void setProductDescrption(String productDescrption) {
        this.productDescrption = productDescrption;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    @Override
    public String toString() {
        return "ProductModel{" + "productId=" + productId + ", productName=" + productName + ", productCode=" + productCode + ", productPrice=" + productPrice + ", storeName=" + storeName + ", brandName=" + brandName + ", categoryNames=" + categoryNames + ", productDescrption=" + productDescrption + ", memberId=" + memberId + '}';
    }
}
