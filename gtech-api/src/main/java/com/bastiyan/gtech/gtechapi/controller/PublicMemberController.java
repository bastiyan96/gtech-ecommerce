/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bastiyan.gtech.gtechapi.controller;

import com.bastiyan.gtech.gtechapi.endpoint.MemberService;
import com.bastiyan.gtech.gtechgateway.model.LoginRequest;
import com.bastiyan.gtech.gtechgateway.model.LoginResponse;
import com.bastiyan.gtech.gtechgateway.model.MemberRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author iBas
 */
@RestController
@RequestMapping("/public/member")
public class PublicMemberController {
    private static final Logger log = LoggerFactory.getLogger(PublicMemberController.class);
        
    
    @Autowired
    private MemberService memberService;
    
    @PostMapping(path = "/register", consumes = "application/json", produces = "application/json")
    public MemberRequest register(@RequestBody MemberRequest memberRegistrationModel) {
        return memberService.register(memberRegistrationModel);
    }

    @PostMapping(path = "/login", consumes = "application/json", produces = "application/json")
    public LoginResponse login(@RequestBody LoginRequest loginModelRequest) {
        log.debug("### LOGIN ###");
        return memberService.login(loginModelRequest);
    }
    
    
}
