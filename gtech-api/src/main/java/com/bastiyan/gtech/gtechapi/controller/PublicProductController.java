/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bastiyan.gtech.gtechapi.controller;

import com.bastiyan.gtech.gtechapi.endpoint.ProductService;
import com.bastiyan.gtech.gtechgateway.model.ProductModel;
import com.bastiyan.gtech.gtechgateway.model.ProductModelWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author iBas
 */
@RestController
@RequestMapping("/public/product")
public class PublicProductController {
    @Autowired
    private ProductService productService;
    
    @GetMapping(consumes = "application/json", produces = "application/json")
    public ProductModelWrapper getProductList() {
        return productService.getProductList();
    }
    
    @GetMapping(path = "/detail/{productId}", consumes = "application/json", produces = "application/json")
    public ProductModel getProductDetail(@PathVariable(value = "productId") String productId) {
        return productService.getProductById(productId);
    }
}
