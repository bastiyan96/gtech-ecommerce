/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bastiyan.gtech.gtechapi.controller;

import com.bastiyan.gtech.gtechapi.endpoint.ProductService;
import com.bastiyan.gtech.gtechgateway.model.ProductModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author iBas
 */
@RestController
@RequestMapping("/private/product")
public class PrivateProductController {
    private static final Logger log = LoggerFactory.getLogger(PrivateProductController.class);
    
    @Autowired
    private ProductService productService;
    
    @PostMapping(consumes = "application/json", produces = "application/json")
    public ProductModel register(@RequestBody ProductModel productModel) {
        return productService.register(productModel);
    }
    
    @PostMapping(path = "/update", consumes = "application/json", produces = "application/json")
    public ProductModel update(@RequestBody ProductModel productModel) {
        return productService.update(productModel);
    }
}
