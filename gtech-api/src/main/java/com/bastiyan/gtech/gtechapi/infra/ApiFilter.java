/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bastiyan.gtech.gtechapi.infra;

import com.bastiyan.gtech.gtechapi.endpoint.MemberService;
import com.bastiyan.gtech.gtechgateway.model.MemberSessionModel;
import com.google.common.base.Strings;
import java.io.IOException;
import java.util.Enumeration;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

/**
 *
 * @author iBas
 */
@Configuration
@Order(Ordered.HIGHEST_PRECEDENCE)
public class ApiFilter implements Filter {

    private static final Logger log = LoggerFactory.getLogger(ApiFilter.class);

    @Autowired
    private MemberService memberService;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        log.info("########## Initiating Custom filter ##########");
    }

    @Override
    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        String method = httpRequest.getMethod();
        String resourcePath = getResourcePath(httpRequest);

        log.info("doFilter {} {}", method, resourcePath);

        Enumeration headerNames = httpRequest.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String headerName = (String) headerNames.nextElement();
            log.info("header: {} - {}", headerName, httpRequest.getHeader(headerName));
        }

        if (resourcePath.startsWith("/public/")) {
            log.info("{} is public API", resourcePath);
            chain.doFilter(request, response);
            return;
        }

        String authorization = httpRequest.getHeader("sessionId");
        log.info("sessionId: {}", authorization);
        if (Strings.isNullOrEmpty(authorization)) {
            httpResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            return;
        }

        try {
            MemberSessionModel session = memberService.validateSession(authorization);
            if (session != null) {
                chain.doFilter(request, response);
                return;
            }
            httpResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Failed Authorization, {}", e.getMessage());
            httpResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }

    private String getResourcePath(HttpServletRequest servletRequest) {
        if (servletRequest.getContextPath() != null && !servletRequest.getContextPath().equals("/")) {
            return servletRequest.getRequestURI().substring(servletRequest.getContextPath().length());
        } else {
            return servletRequest.getRequestURI();
        }
    }
}
