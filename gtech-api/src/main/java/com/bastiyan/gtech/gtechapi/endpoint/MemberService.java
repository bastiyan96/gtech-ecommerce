/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bastiyan.gtech.gtechapi.endpoint;

import com.bastiyan.gtech.gtechapi.controller.PrivateMemberController;
import com.bastiyan.gtech.gtechgateway.model.LoginRequest;
import com.bastiyan.gtech.gtechgateway.model.LoginResponse;
import com.bastiyan.gtech.gtechgateway.model.MemberRequest;
import com.bastiyan.gtech.gtechgateway.model.MemberResponse;
import com.bastiyan.gtech.gtechgateway.model.MemberSessionModel;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import static org.springframework.web.servlet.function.RequestPredicates.param;

/**
 *
 * @author iBas
 */
@Service
public class MemberService {
    private static final Logger log = LoggerFactory.getLogger(MemberService.class);

    @Value("${core.endpoint}")
    private String coreEndpoint;

    @Autowired
    private RestTemplate restTemplate;

    public LoginResponse login(LoginRequest loginRequest) {
        LoginResponse result = restTemplate.postForObject(coreEndpoint + "/member/login", loginRequest, LoginResponse.class);
        return result;
    }

    public MemberRequest register(MemberRequest memberRegistrationModel) {
        MemberRequest result = restTemplate.postForObject(coreEndpoint + "/member/register", memberRegistrationModel, MemberRequest.class);
        return result;
    }

    public MemberResponse viewProfile(String memberId) {
        HttpEntity entity = new HttpEntity(buildDefaultHeaders());
        
        Map<String, String> params = new HashMap<String, String>();
        params.put("memberId", memberId);

        ResponseEntity<MemberResponse> response = restTemplate.exchange(
                coreEndpoint + "/member/{memberId}", HttpMethod.GET, entity, MemberResponse.class, params);

        return response.getBody();
    }
    
    public MemberResponse update(MemberRequest memberRequest) {
        log.info("## memberRegistrationModel.getMemberId is {} ##", memberRequest.getMemberId());
        MemberResponse result = restTemplate.postForObject(coreEndpoint + "/member/update", memberRequest, MemberResponse.class);
        return result;
    }

    public MemberSessionModel getSession(String sessionId) {
        HttpEntity entity = new HttpEntity(buildDefaultHeaders());
        
        Map<String, String> params = new HashMap<String, String>();
        params.put("sessionId", sessionId);

        ResponseEntity<MemberSessionModel> response = restTemplate.exchange(
                coreEndpoint + "/member/session/get/{sessionId}", HttpMethod.GET, entity, MemberSessionModel.class, params);

        return response.getBody();
    }

    public MemberSessionModel validateSession(String sessionId) {
        HttpEntity entity = new HttpEntity(buildDefaultHeaders());
        
        Map<String, String> params = new HashMap<String, String>();
        params.put("sessionId", sessionId);

        ResponseEntity<MemberSessionModel> response = restTemplate.exchange(
                coreEndpoint + "/member/session/validate/{sessionId}", HttpMethod.GET, entity, MemberSessionModel.class, params);

        return response.getBody();
    }
    
    private HttpHeaders buildDefaultHeaders(){
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }
}
