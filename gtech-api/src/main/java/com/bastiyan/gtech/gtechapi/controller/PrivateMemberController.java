/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bastiyan.gtech.gtechapi.controller;

import com.bastiyan.gtech.gtechapi.endpoint.MemberService;
import com.bastiyan.gtech.gtechgateway.model.MemberRequest;
import com.bastiyan.gtech.gtechgateway.model.MemberResponse;
import com.bastiyan.gtech.gtechgateway.model.MemberSessionModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author iBas
 */
@RestController
@RequestMapping("/private/member")
public class PrivateMemberController {
    private static final Logger log = LoggerFactory.getLogger(PrivateMemberController.class);
    
    @Autowired
    private MemberService memberService;
    
    @GetMapping(consumes = "application/json", produces = "application/json")
    public MemberResponse myProfile(@RequestHeader("sessionId") String sessionId) {
        MemberSessionModel session = memberService.getSession(sessionId);
        return memberService.viewProfile(session.getMemberId());
    }
    
    @PostMapping(path = "/update", consumes = "application/json", produces = "application/json")
    public MemberResponse updateProfile(@RequestBody MemberRequest request) {
        log.info("## memberRegistrationModel.getMemberId is {} ##", request.getMemberId());
        return memberService.update(request);
    }
    
}
