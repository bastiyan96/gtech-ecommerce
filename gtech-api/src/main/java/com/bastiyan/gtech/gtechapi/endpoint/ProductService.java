/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bastiyan.gtech.gtechapi.endpoint;

import com.bastiyan.gtech.gtechgateway.model.ProductModel;
import com.bastiyan.gtech.gtechgateway.model.ProductModelWrapper;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author iBas
 */
@Service
public class ProductService {

    @Value("${core.endpoint}")
    private String coreEndpoint;

    @Autowired
    private RestTemplate restTemplate;

    public ProductModel register(ProductModel productModel) {
        ProductModel result = restTemplate.postForObject(coreEndpoint + "/product", productModel, ProductModel.class);
        return result;
    }

    public ProductModel update(ProductModel productModel) {
        ProductModel result = restTemplate.postForObject(coreEndpoint + "/product/update", productModel, ProductModel.class);
        return result;
    }

    public ProductModelWrapper getProductList() {
        HttpEntity entity = new HttpEntity(buildDefaultHeaders());

        ResponseEntity<ProductModelWrapper> response = restTemplate.exchange(
                coreEndpoint + "/product", HttpMethod.GET, entity, ProductModelWrapper.class);

        return response.getBody();
    }

    public ProductModel getProductById(String productId) {
        HttpEntity entity = new HttpEntity(buildDefaultHeaders());

        Map<String, String> params = new HashMap<String, String>();
        params.put("productId", productId);

        ResponseEntity<ProductModel> response = restTemplate.exchange(
                coreEndpoint + "/product/{productId}", HttpMethod.GET, entity, ProductModel.class, params);

        return response.getBody();
    }

    private HttpHeaders buildDefaultHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }
}
